#!/usr/bin/python3

import urllib.request as url
import json
import codecs
import sys


class GeoIP:


	def __init__(self):
		pass


	@classmethod
	def Info(cls, ip):

		"""Display json format information of a public ip address."""

		if isinstance(ip, str):
			try:
				version = sys.version_info

				if version[0] == 3:
					if version[1] >= 7:
						request = url.Request(f"https://ipapi.co/{ip}/json/", headers={"User-Agent" : "Googlebot"})
					else:
						request = url.Request("https://ipapi.co/{}/json/".format(ip), headers={"User-Agent" : "Googlebot"})

					response = url.urlopen(request)
					reader = codecs.getreader("utf-8")
				
					data = json.load(reader(response))

					if isinstance(data, dict):
						return data
			except:
				pass
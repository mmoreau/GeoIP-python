import os
import sys
from GeoIP import GeoIP

# 8.8.8.8 => Google Primary DNS Server
data = GeoIP.Info("8.8.8.8")

if data:
	if "error" not in data.keys():
		for key, values in data.items():
			print(key, values)

if sys.platform.startswith("win32"):
	os.system("pause")
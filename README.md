<h1>GeoIP</h1>
<p>Retrieve information about an ip address via an API. The answer is returned as JSON.</p>

<h2><b>Script</b></h2>
<p>Geo.Info(IP)</p>
<ul>
    <Li>IP : IPv4 / IPv6</li>
</ul>

<p>Consult the "script.py" file to understand the examples</p>

<h2>Contact me</h2>
<p>Feel free to contact me if you have any questions, criticisms, suggestions, bugs encountered </p>

<p><b>Mail :</b> mx.moreau1@gmail.com<p>
<p><b>Twitter :</b> mxmmoreau (https://twitter.com/mxmmoreau)</p>